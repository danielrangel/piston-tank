use piston_window::*;
use gfx_device_gl::Resources;
use std::f64::consts::PI;
use graphics::math::Matrix2d;
use nc::query::PointQuery;
use na::Isometry2;

use object::Pnt2;
use object::{Object, Cuboid2f, Vec2};
use object::component::Component;
use bullet::Bullet;

pub struct Tank {
    pub hull: Component,
    pub turret: Component,
    pub is_destroyed: bool,
    collider: Cuboid2f,
    point_to: Vec2,
}

impl Tank {
    pub fn new() -> Tank {
        Tank {
            hull: Component::new(),
            turret: Component::new(),
            is_destroyed: false,
            collider: Cuboid2f::new(Vec2::new(38.0, 65.0)),
            point_to: Vec2::new(0.0, 0.0),
        }
    }

    pub fn point_tur_to(&mut self, x: f64, y: f64) {
        self.point_to = Vec2::new(x, y);
    }

    pub fn calc_tur_pos(&mut self, dt: f64) {
        let mut new_rot =
            (-(self.point_to.x - self.hull.trans.pos.x)).atan2(self.point_to.y -
                                                               self.hull.trans.pos.y);
        if new_rot == self.turret.trans.rot {
            return;
        }
        if new_rot < self.turret.trans.rot &&
           self.turret.trans.rot - new_rot > new_rot + 2.0 * PI - self.turret.trans.rot {
            new_rot += 2.0 * PI;
        }
        if new_rot > self.turret.trans.rot &&
           new_rot - self.turret.trans.rot > self.turret.trans.rot + 2.0 * PI - new_rot {
            new_rot -= 2.0 * PI;
        }
        let rot_speed = 1.0;
        if new_rot > self.turret.trans.rot {
            if new_rot - self.turret.trans.rot > rot_speed * dt {
                self.turret.trans.rot += rot_speed * dt;
            } else {
                self.turret.trans.rot = new_rot;
            }
        } else {
            if self.turret.trans.rot - new_rot > rot_speed * dt {
                self.turret.trans.rot -= rot_speed * dt;
            } else {
                self.turret.trans.rot = new_rot;
            }
        }
        if self.turret.trans.rot > 2.0 * PI {
            self.turret.trans.rot -= 2.0 * PI;
        }
        if self.turret.trans.rot < 0.0 {
            self.turret.trans.rot += 2.0 * PI;
        }
    }
    pub fn collides(&mut self, b: &Bullet) -> bool {
        let bpnt = Pnt2::new(b.bullet.trans.pos.x, b.bullet.trans.pos.y);
        self.collider
            .contains_point(&Isometry2::new(self.hull.trans.pos, 0.0), &bpnt)
    }
    pub fn fire(&self, sprite: Texture<Resources>) -> Bullet {
        let mut bul = Bullet {
            bullet: Component::new(),
            to_be_removed: false,
        };
        bul.mov_to(self.turret.trans.pos);
        bul.rot_to(self.turret.trans.rot);
        bul.fwd(125.0);
        bul.bullet.set_sprite(sprite);
        bul
    }
}
impl Object for Tank {
    fn mov(&mut self, pos: Vec2) {
        self.hull.trans.mov(pos);
        self.turret.trans.mov(pos);
    }
    fn mov_to(&mut self, pos: Vec2) {
        self.hull.trans.mov_to(pos);
        self.turret.trans.mov_to(pos);
    }
    fn rot(&mut self, r: f64) {
        self.hull.trans.rot(r);
        self.turret.trans.rot(r);
    }
    fn rot_to(&mut self, r: f64) {
        self.turret.trans.rot(r - self.hull.trans.rot);
        self.hull.trans.rot_to(r);
    }
    fn fwd(&mut self, d: f64) {
        self.hull.trans.fwd(d);
        self.turret.trans.pos = self.hull.trans.pos;
    }
    fn update(&mut self, dt: f64) {
        self.turret.trans.pos = self.hull.trans.pos;
        self.calc_tur_pos(dt);
    }
    fn render(&mut self, v: Matrix2d, g: &mut G2d, ) {
        self.hull.render(v, g);
        self.turret.render(v, g);
    }
}
